#!/usr/bin/env bats

@test "online" {
  wait-for-it --timeout=5 --strict app:9000
  wait-for-it --timeout=5 --strict static:80
}

@test "healty" {
  http --check-status app:9000/health
}

@test "Static image is present" {
  http --check-status GET http://static:80/images/background.jpg
}

@test "Resize with URL source" {
  http --check-status GET app:9000/resize width==500 height==400 url==http://static:80/images/background.jpg
}
