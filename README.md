# Imagine

An image processor.
https://imagine.nexylan.dev

## Usage

```
$ curl https://imagine.nexylan.dev
{
  "imaginary": "0.1.28",
  "bimg": "1.0.5",
  "libvips": "8.4.1"
}
```

Demo page: https://imagine.nexylan.dev/form

The h2non/imaginary API provide multiple endpoints to convert and manipulate popular image formats.

All are well documented on the [h2non/imaginary project website](https://github.com/h2non/imaginary#http-api).

It supports [multiple image operations](https://github.com/h2non/imaginary#supported-image-operations).

### Cookbook

Example may be added here.
If you want to contribute, please put example using [HTTPie](https://httpie.org/) for better readability.

#### Convert a local image to an another format

Fill `type` with the supported format you want.

```
http --form POST https://imagine.nexylan.dev/convert \
	stripmeta==true \
	lossless==true \
	type==webp \
	file@images/background.jpg \
  > images/background.webp
```
