FROM registry.gitlab.com/nexylan/docker/core:1.12.1-alpine
RUN apk add --no-cache bats~=1
COPY tests.bats .
CMD ["bats", "."]
